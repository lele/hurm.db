.. -*- coding: utf-8 -*-
.. :Project:   hurm -- Documentation master file
.. :Created:   Tue Jan 12 11:20:41 2016
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2016 Lele Gaifax
..

Human Resources Management's documentation
==========================================

Contents:

.. toctree::
   :maxdepth: 2

   database/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

