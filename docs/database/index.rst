.. -*- coding: utf-8 -*-
.. :Project:   hurm
.. :Created:   mar 12 gen 2016 11:27:50 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2016 Lele Gaifax
..

Database definition
===================

.. toctree::
   :maxdepth: 2

   domains
   tables/index


.. patchdb:script:: Public schema permissions
   :mimetype: text/x-postgresql

   GRANT USAGE ON SCHEMA public TO PUBLIC
