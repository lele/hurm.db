.. -*- coding: utf-8 -*-
.. :Project:   hurm -- Database tables definitions
.. :Created:   mar 12 gen 2016 12:27:29 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2016 Lele Gaifax
..

===================
 Tables definition
===================

.. toctree::
   :maxdepth: 2

   activities
   activitypayloads
   availabilities
   duties
   dutypayloads
   editions
   locations
   persons
   personactivities
   tasks
