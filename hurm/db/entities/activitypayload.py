# -*- coding: utf-8 -*-
# :Project:   hurm -- ActivityPayload class, mapped to table activitypayloads
# :Created:   sab 27 feb 2016 16:52:55 CET
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2016 Lele Gaifax
#


from . import AbstractBase


class ActivityPayload(AbstractBase):
    def __str__(self):
        return self.description
