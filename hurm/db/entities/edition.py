# -*- coding: utf-8 -*-
# :Project:   hurm -- Edition class, mapped to table editions
# :Created:   sab 02 gen 2016 15:34:14 CET
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2016 Lele Gaifax
#

from . import AbstractBase


class Edition(AbstractBase):
    def __str__(self):
        return self.description
