# -*- coding: utf-8 -*-
# :Project:   hurm -- Location class, mapped to table locations
# :Created:   sab 02 gen 2016 15:34:26 CET
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2016 Lele Gaifax
#

from . import AbstractBase


class Location(AbstractBase):
    def __str__(self):
        return self.description
