# -*- coding: utf-8 -*-
# :Project:   hurm -- Task class, mapped to table tasks
# :Created:   sab 02 gen 2016 15:34:46 CET
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2016 Lele Gaifax
#

from . import AbstractBase


class Task(AbstractBase):
    def __str__(self):
        return '%s, %s, %s' % (self.location, self.activity, self.date)
