# -*- coding: utf-8 -*-
# :Project:   hurm -- Availability class, mapped to table availabilities
# :Created:   sab 02 gen 2016 15:33:19 CET
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2016 Lele Gaifax
#

from . import AbstractBase


class Availability(AbstractBase):
    def __str__(self):
        return '%s, %s' % (self.person, self.date)
