# -*- coding: utf-8 -*-
# :Project:   hurm
# :Created:   mar 22 dic 2015 16:04:16 CET
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2015, 2016 Lele Gaifax
#

from .activities import activities
from .activitypayloads import activitypayloads
from .availabilities import availabilities
from .duties import duties
from .dutypayloads import dutypayloads
from .editions import editions
from .locations import locations
from .personactivities import personactivities
from .persons import persons, password_manager
from .tasks import tasks
