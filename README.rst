.. -*- coding: utf-8 -*-
.. :Project:   hurm -- Human Resources Manager
.. :Created:   lun 14 dic 2015, 10.24.47, CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2015, 2016 Lele Gaifax
..

=========================
 Human Resources Manager
=========================

Database definition and SQLAlchemy modelization
===============================================

 :author: Lele Gaifax
 :contact: lele@metapensiero.it
 :license: GNU General Public License version 3 or later

This module describes the PostgreSQL database used by HuRM and implements its SQLAlchemy
modelization.
