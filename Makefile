# -*- coding: utf-8 -*-
# :Project:   hurm -- Master Makefile
# :Created:   lun 14 dic 2015, 10.24.47, CET
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2015, 2016 Lele Gaifax
#

DBSRC := hurm/db

all: help

include Makefile.i18n
include Makefile.database
include Makefile.release
