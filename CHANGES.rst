.. -*- coding: utf-8 -*-

Changes
-------

0.10 (2016-03-24)
~~~~~~~~~~~~~~~~~

- Re-release, PyPI can be a PITA...


0.9 (2016-03-24)
~~~~~~~~~~~~~~~~

- Remove utils.dump(), moved to metapensiero.sqlalchemy.dbloady


0.8 (2016-03-04)
~~~~~~~~~~~~~~~~

- Fix deletion of persons, when associated to activities


0.7 (2016-03-01)
~~~~~~~~~~~~~~~~

- Add generic payloads to activities and duties


0.6 (2016-02-23)
~~~~~~~~~~~~~~~~

- Promote the changes logger to the top level


0.5 (2016-02-21)
~~~~~~~~~~~~~~~~

- Handle the case when all preferred activities are removed


0.4 (2016-02-20)
~~~~~~~~~~~~~~~~

- Add person's preferred activities

- Fix SQLAlchemy description of table tasks


0.3 (2016-02-17)
~~~~~~~~~~~~~~~~

- Dump facility, usable to produce a YAML archive compatible with
  metapensiero.sqlalchemy.dbloady


0.2 (2016-02-16)
~~~~~~~~~~~~~~~~

- Stable enough for the first demo


0.1 (unreleased)
~~~~~~~~~~~~~~~~

- Initial effort
